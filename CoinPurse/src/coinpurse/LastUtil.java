package coinpurse;
import java.util.Arrays;
import java.util.List;


public class LastUtil {
	/**
	 * printList is method that Print String form StringList method.
	 * @param list is List of type String 
	 */
	public static void printList(List<String> list){
	   System.out.println(StringList(list,0));
	}
	/**
	 * StringList is method that get String from list
	 * @param list is List of type String 
	 * @param index is integer of index defined about list
	 * @return String text about content from list
	 */
	public static String StringList(List<String> list,int index){
		if(index>list.size()-1) return "";
		String text = list.get(index)+" "+StringList(list,index+1);
		return text;
	}
	/**
	 * max is method that return Max of String in the list
	 * @param list is List of type String 
	 * @return the String from findMax method from list
	 */
	public static String max(List<String> list){
		return findMax(list,0,list.get(0));	
	}
	/**
	 * findMax is method that return String is maximum value in list
	 * @param list is List of type String
	 * @param index is integer that defined the index of list
	 * @param max is String that maximum value then 
	 * @return String that value is maximum of list
	 */
	public static String findMax(List<String> list, int index,String max){
       if(index>=list.size()) return max;
       if(max.compareTo(list.get(index))<0) max = list.get(index);
       return findMax(list,index+1,max);
	}
	/**
	 * main method is runnable to the programming 
	 * @param args
	 */
	public static void main(String [] args) {
		List<String> list;
		if (args.length > 0) list = Arrays.asList( args );
		else list = Arrays.asList("bird", "zebra", "cat", "pig");
		System.out.print("List contains: ");
		printList( list );
		String max = max(list);
		System.out.println("Lexically greatest element is "+max);
	}

}

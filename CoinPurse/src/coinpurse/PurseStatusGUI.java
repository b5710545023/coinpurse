package coinpurse;

import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
/**
 * PurseStatusGUI is class of Graphical User Interface that show status of this purse.
 * @author Kundjanasith Thonglek
 * @version 24.02.2015
 */
public class PurseStatusGUI implements Observer{
	/** Frame of this Purse Status GUI	 */
	private JFrame frame;
	/**	ProgressBar that show status in pattern bar */
	private JProgressBar pro;
	/**Label that show status of this purse */
	private JLabel status;
	/**
	 * Constructor of PurseStatusGUI that set the in it component
	 */
	public PurseStatusGUI(){
		this.init();
	}
	/**
	 * run is method that runnable the PurseStatusGUI
	 */
	public void run(){
		frame.setVisible(true);
	}
	/**
	 * init is method that set the PurseStatusGUI
	 */
	public void init(){
		frame = new JFrame();
		frame.setTitle("Purse Status");
		frame.setResizable(true);
		frame.setBounds(100,100,500,110);
		frame.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width/2-frame.getSize().width/2,Toolkit.getDefaultToolkit().getScreenSize().height/2-frame.getSize().height/2);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel pane = new JPanel();
		pane.setLayout(new GridLayout(2,1));
		pro = new JProgressBar();
		status = new JLabel("Status");
		pane.add(status);
		pane.add(pro);
		frame.add(pane);
	}
	/**
	 * update is method that update receive notification from the purse
	 * @param subject is Observable
	 * @param info is Object
	 */
	@Override
	public void update(Observable subject, Object info) {
		if (subject instanceof Purse) {
			Purse purse = (Purse)subject;
			int capacity = purse.getCapacity();
			int count = purse.count();
			pro.setMaximum(capacity);
			pro.setValue(count);
			if(count==capacity)status.setText("FULL");
			else if(count==0)status.setText("EMPTY");
			else status.setText(count+"");
		}
		if (info != null) System.out.println( info );
	}
}

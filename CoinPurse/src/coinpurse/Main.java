package coinpurse;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Kundjanasith Thonglek
 */  

public class Main{
	private static int CAPACITY = 10;
	/** main is method that program will run.
	 * @param args not used
	 */
	public static void main( String[] args ) {
		PurseBalanceGUI pursebalance = new PurseBalanceGUI();
		pursebalance.run();
		PurseStatusGUI pursestatus = new PurseStatusGUI();
		pursestatus.run();
		WithdrawStrategy greedy = new RecursiveWithdraw(); 
		Purse purse = new Purse(CAPACITY);
		purse.addObserver(pursebalance);
		purse.addObserver(pursestatus);
		purse.setWithdrawStrategy(greedy);
		ConsoleDialog ui = new ConsoleDialog( purse );
		ui.run();
	}
}

package coinpurse;
/**
 * AbstractValuable is superclass of Coin,Coupon,Banknote.
 * contain equals method and compareTo method
 * @author Kundjanasith Thonglek
 */   
public abstract class AbstractValuable implements Valuable{
	/** Value of the Valuable.*/
	private double value;
	public AbstractValuable(double value) {
		this.value=value;
	}  
	/**
	 * equals is method for check equals  this valuable with valuable by value.
	 * @return true 
	 * @param obj is another object to compare
	 * if another object is not null.
	 * if another object is valuable.
	 * if value of this is equals value of another valuable.
	 */
	public boolean equals(Object obj) {
		if(obj==null)return false;
		if(obj.getClass()!=this.getClass())return false;
		if(((Valuable)obj).getValue()==this.getValue())return true;
		return false;
	}
	/**   
	 * compareTo is method for compare this valuable with another valuable.
	 * @param obj is a valuable to compare to this
	 * @return -1 if this valuable has lower value
	 * @throws NullPointerException if valuable is null
	 * @see java.lang.Comparable#compareTo(Valuable)
	 */
	public int compareTo(Valuable other){
		if(other==null)return -1;
		if(this.getValue()<other.getValue()) return  -1;
		else if(this.getValue()>other.getValue()) return 1;
		else return 0;
	}
	/**
	 * Get a value of Valuable.
	 * @return value of the Valuable
	 */
	public double getValue(){
		return this.value;
	}
}
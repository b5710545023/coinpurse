package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;
/**
 * withdrawstrategy is interface that have withdraw method 
 * @author Kundjanasith Thonglek
 */
public interface WithdrawStrategy {
	/**
	 * withdraw method is about withdraw money  
	 * @param amount is value of money.
	 * @param valuables is list of Valuable.
	 * @return
	 */
	Valuable[] withdraw(double amount,List<Valuable> valuables );
}

package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;
/**
 * greedywithdram is class that greedy process withdraw and realize with withdrawstrategy.
 * @author Kundjanasith Thonglek
 */
public class GreedyWithdraw implements WithdrawStrategy {
 
	/**
	 * withdraw is method that withdraw money from the purse.
	 * @param amount is double that value of purse
	 * @param valuables is list that type of Valuable
	 */
	@Override
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		if(amount<0) return null;
		ArrayList <Valuable>temp = new ArrayList<Valuable>();

		for( int i=valuables.size()-1 ; i>=0 ; i--){
			if(amount - valuables.get(i).getValue() >= 0){
				temp.add(valuables.get(i));
				amount-=valuables.get(i).getValue();
			}
		}
		Valuable[] array = new Valuable[temp.size()];
		temp.toArray(array);
		if ( amount > 0 )
		{
			return  null;
		}
		
		return array;
	}

}

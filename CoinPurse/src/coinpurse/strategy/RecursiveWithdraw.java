package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;

/**
 * recursivewithdraw is class that recursive process withdraw and realize with withdrawstrategy.
 * @author Kundjanasith Thonglek
 */
public class RecursiveWithdraw implements WithdrawStrategy {
	/**
	 * withdraw is method that withdraw money from the purse.by recursion
	 * @param amount is double that value of purse
	 * @param valuables is list that type of Valuable
	 */
	@Override
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		List<Valuable> value = withdrawFrom(amount, valuables,valuables.size() - 1);
		if (value != null)
			return value.toArray(new Valuable[0]);
		return null;
	}
    /**
     * withdrawFrom is the helper method for recursion of recursivewithdraw.
     * @param amount is double that value of purse
     * @param valuables is list that type of Valuable
     * @param lastIndex is index from the last of list valuables
     * @return
     */
	private List<Valuable> withdrawFrom(double amount,List<Valuable> valuables, int lastIndex) {
		if (lastIndex < 0)
			return null;
		if (amount < 0)
			return null;
		List<Valuable> temp = new ArrayList<Valuable>();
		if (amount - valuables.get(lastIndex).getValue() == 0) {
			temp.add(valuables.get(lastIndex));
			return temp;
		}
		List<Valuable> temp2 = new ArrayList<Valuable>();
		temp2 = withdrawFrom(amount, valuables, lastIndex - 1);
		if (temp2 == null) {
			temp2 = withdrawFrom(amount - valuables.get(lastIndex).getValue(),valuables,lastIndex - 1);
			if (temp2 != null){
				temp2.add(valuables.get(lastIndex));
			}
		}
		return temp2;
	}

}

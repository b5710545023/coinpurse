package coinpurse;
import java.util.Hashtable;
import java.util.Map;
import java.util.HashMap;
/**
 * A coupon with a monetary value.
 * You can't change the color of a coupon.
 * @author Kundjanasith Thonglek
 */
public class Coupon extends AbstractValuable{
	/** Color of the coupon.*/
	private String color;
	private static Map<String,Double> colors;
	static{
		colors = new Hashtable<String,Double>();
		colors.put("RED",new Double(100.00));
		colors.put("BLUE",new Double(50.00));
		colors.put("GREEN",new Double(20.00));
	}
	/** 
	 * Constructor for a new coupon. 
	 * @param color is the color for the coupon
	 */
	public Coupon(String color){
		super(colors.get(color.toUpperCase()));
		this.color=color;
	}
	/**
	 * toString is method for print out value of this coupon.
	 * @return value of this coupon + " coupon"
	 */
	public String toString(){
		return this.color+" coupon";
	}

}

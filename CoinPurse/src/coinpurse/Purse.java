package coinpurse; 

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import coinpurse.strategy.WithdrawStrategy;


/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  @author Kundjanasith Thonglek
 */
public class Purse extends java.util.Observable{

	/** Collection of coins in the purse. */
	public List<Valuable> money = new ArrayList<Valuable>();
	/** Value of Comparator that is comparator.     */
	private final ValueComparator comparator= new ValueComparator();
	/** Capacity is maximum NUMBER of coins the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;
	/** Strategy is withdrawstrategy of purse.     */
	private WithdrawStrategy strategy;

	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of coins you can put in purse.
	 */
	public Purse( int capacity ) {
		this.money = new ArrayList<Valuable>();
		this.capacity = capacity;
	}
	/**
	 * Count and return the number of coins in the purse.
	 * This is the number of coins, not their value.
	 * @return the number of coins in the purse
	 */
	public int count() {
		return this.money.size();
	}

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() {
		double total = 0;
		for (int i=0 ; i<this.money.size() ; i++){
			if(this.money.get(i)==null){
				total+=0;
			}
			else{
				total+=this.money.get(i).getValue();
			}
		}
		return total; 
	}

	/**
	 * Return the capacity of the coin purse.
	 * @return the capacity
	 */
	public int getCapacity() { 
		return this.capacity;
	}

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
		if(this.getCapacity()==this.count()) return true;
		return false;
	}

	/** 
	 * Insert a coin into the purse.
	 * The coin is only inserted if the purse has space for it
	 * and the coin has positive value.  No worthless coins!
	 * @param value is value object to insert into purse
	 * @return true if coin inserted, false if can't insert
	 */
	public boolean insert( Valuable value ) {
		if(!this.isFull()&&value.getValue()!=0){
			this.money.add(value);
			super.setChanged();
			super.notifyObservers(this);
			return true;
		}
		else{
			return false;
		}
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Coins withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Coin objects for money withdrawn,or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		Collections.sort(this.money, comparator);
		Valuable [] array = strategy.withdraw(amount,this.money);
		if(amount<0) return null;
		if(amount==0) return new Valuable[this.capacity];
		if(array==null) return null;
		else {
			int size = array.length-1;
			for(int i=0 ; i<this.count() ; i++){
				for(int j=size ; j>=0 ; j--){
					if(this.money.get(i).getValue()==array[j].getValue()){
						this.money.remove(i);
						size = j-1;
						i--;
						break;
					}
				}
			}
		}
		super.setChanged();
		super.notifyObservers(this);
		return array;

	}

	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 * @return amount of coins and value of coins
	 */
	public String toString() {
		int z = this.count();
		double value = 0 ;
		for(int i=0 ; i<this.money.size() ; i++){
			value += this.money.get(i).getValue();
		}
		return  String.format("%d coins with value %.1f",z,value);
	}

	/**
	 * setStrategy is method that set WithdrawStrategy of this.
	 * @param strategy
	 */
	public void setWithdrawStrategy(WithdrawStrategy strategy){
		this.strategy=strategy;
	}

	
}

package coinpurse;
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Kundjanasith Thonglek
 */
public class Coin extends AbstractValuable{

	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 */
	public Coin( double value ) {
	     super(value);
	}
	/**
	 * toString is method for print out value of this coin.
	 * @return value of this coin + "-Baht coin"
	 */
	public String toString(){
		return String.format("%.0f-Baht coin",super.getValue());
	}
}


package coinpurse;
/**
 * A banknote with a monetary value.
 * You can't change the value of a banknote.
 * You can't change the serialNumber of banknote;
 * @author Kundjanasith Thonglek
 */
public class BankNote extends AbstractValuable  {
	/** Serial of the banknote.*/
	private static long SerialNumber = 1000000;
	/** 
	 * Constructor for a new banknote. 
	 * @param value is the value for the banknote
	 */
	public BankNote (int value){
		super(value);
		this.SerialNumber=this.getNextSerialNumber();
	}
	/**
	 * toString is method for print out value of this coin.
	 * @return value of this banknote + "-Baht banknote"
	 */
	public String toString(){
		return String.format("%.0f-Baht Banknote [%d]",super.getValue(),this.SerialNumber);
	}
	/**
	 * getNextSerialNumber is method for get SerialNumber plus one.
	 * @return this.SerialNUmber plus one
	 */
	public long getNextSerialNumber(){
		return this.SerialNumber+=1;
	}
}

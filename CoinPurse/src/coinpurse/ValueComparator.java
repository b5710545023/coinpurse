package coinpurse;
import java.util.Comparator;
/**
 * A ValueComparator is class. that implements Comparator.
 * @author Kundjanasith Thonglek
 */
public class ValueComparator implements Comparator<Valuable>{
	/**
	* Compare a and b.
	* @param a is Valuable that compared
	* @param b is Valuable that compared
	* @return < 0 if a should be "before" b,
	* > 0 if a should be "after" b,
	* = 0 if a and b have same lexical order.
	*/
	public int compare(Valuable a, Valuable b) {
		if(a.getValue()<b.getValue())return -1;
		else if(a.getValue()>b.getValue())return 1;
		else return 0;
	}
	


}

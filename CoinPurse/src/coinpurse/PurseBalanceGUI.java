package coinpurse;

import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * PurseBalanceGUI is class of Graphical User Interface that show balance of this purse.
 * @author Kundjanasith Thonglek
 * @version 24.02.2015
 */
public class PurseBalanceGUI implements Observer{
	/** Frame of this Purse Balance GUI	 */
	private JFrame frame;
	/** Label that show balance of this purse  */
	private JLabel Balance;
	/**
	 * Constructor of PurseBalanceGUI that set the in it component
	 */
	public PurseBalanceGUI(){
		this.init();
	}
	/**
	 * run is method that runnable the PurseBalanceGUI
	 */
	public void run(){
		frame.setVisible(true);
	}
	/**
	 * init is method that set the PurseBalanceGUI
	 */
	public void init(){
		frame = new JFrame();
		frame.setTitle("Purse Balance");
		frame.setResizable(false);
		frame.setBounds(100,100, 300,110);
		frame.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width/2-frame.getSize().width/2,Toolkit.getDefaultToolkit().getScreenSize().height/2-frame.getSize().height/2);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel pane = new JPanel();
		pane.setLayout(new GridLayout(1,2));
		String balance = "Purse-Balance";
		Balance = new JLabel(String.format("%25s",balance));
		JLabel label2 = new JLabel("             Baht.");
		pane.add(Balance);
		pane.add(label2);
		frame.add(pane);

	}
	/**
	 * update is method that update receive notification from the purse
	 * @param subject is Observable
	 * @param info is Object
	 */
	@Override
	public void update(Observable subject, Object info) {
		if (subject instanceof Purse) {
			Purse purse = (Purse)subject;
			int balance = (int)purse.getBalance();
			Balance.setText(String.format("%25s",balance+""));
			System.out.println("Balance is: " + balance);
		}
		if (info != null) System.out.println( info );

	}
}

package coinpurse;
/**
 * @author Kundjanasith Thonglek
 */
public interface Valuable extends Comparable<Valuable>{
	/**
	 * interface method that return this value.
	 * @return value of this class that implementation.
	 */
    double getValue();
}
